object Versions {
    val kotlin = "1.3.61"
    val androidx = "1.1.0"
    val versionCode = 1
    val versionName = "1.0"
    val min_sdk = 14
    val target_sdk = 29
    val compile_sdk = 29
    val build_tools_version = "29.0.2"

    val constraintLayout = "1.1.3"
    val paging_version = "2.1.1"
    val architecture_components_version = "2.1.0"

    val retrofit = "2.6.2"
    val retrofitLoggingInterceptor = "4.2.2"
    val retrofitAdapter = "2.4.0"
    val picassoImageLoader = "2.71828"

    // Junit Testing version

    val junitVersion = "4.12"

}

object Deps {
    val kotlinStandardLibrary = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val ktx = "androidx.core:core-ktx:${Versions.androidx}"

    val appCompat = "androidx.appcompat:appcompat:${Versions.androidx}"
    val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    val recyclerView = "androidx.recyclerview:recyclerview:${Versions.androidx}"
    val material = "com.google.android.material:material:1.1.0-alpha05"

    val pagingArchComponent = "androidx.paging:paging-runtime-ktx:${Versions.paging_version}"
    val viewmodelArchComponent =
        "androidx.lifecycle:lifecycle-extensions:${Versions.architecture_components_version}"
    val lifecycleCompiler =
        "androidx.lifecycle:lifecycle-compiler:${Versions.architecture_components_version}"

    val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    val retrofitConverterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    val retrofitLoggingInterceptor =
        "com.squareup.okhttp3:logging-interceptor:${Versions.retrofitLoggingInterceptor}"
    val retrofitAdapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitAdapter}"

    val picassoImageLoader = "com.squareup.picasso:picasso:${Versions.picassoImageLoader}"


    // Testing
    val junitTesting = "junit:junit:${Versions.junitVersion}"
    val espresso = "androidx.test.espresso:espresso-core:3.3.0-alpha03"
    val espressoTestRules = "androidx.test:rules:1.3.0-alpha03"
}