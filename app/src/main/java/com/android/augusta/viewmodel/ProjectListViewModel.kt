package com.android.augusta.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.augusta.webservice.model.Project
import com.android.augusta.webservice.repository.ProjectRepository


class ProjectListViewModel(private val mApplication: Application, private val mParam: String) :
    AndroidViewModel(mApplication) {

    private val projectListObservable = ProjectRepository.getInstance().getProjectList(mParam)

    fun getProjectListObservable(): LiveData<List<Project>> {
        return projectListObservable
    }

    class Factory(private val application: Application, private val projectID: String) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return ProjectListViewModel(application, projectID) as T
        }
    }
}



