package com.android.augusta.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.android.augusta.datasource.UserDataSource
import com.android.augusta.datasource.UserDataSourceFactory
import com.android.augusta.utils.Appconstants
import com.android.augusta.webservice.model.User

class UserListViewModel(application: Application) : AndroidViewModel(application) {

    var userPagedList: LiveData<PagedList<User>>
    private var liveDataSource: LiveData<UserDataSource>

    init {
        val itemDataSourceFactory = UserDataSourceFactory()
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(Appconstants.RECORDS_PER_PAGE)
            .build()
        userPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
    }
}