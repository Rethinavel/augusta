package com.android.augusta.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.augusta.webservice.model.User
import com.android.augusta.webservice.repository.UserRepository

  class LoginViewModel(application: Application, mUsername: String) :
    AndroidViewModel(application) {

    private val userObservable = UserRepository.getInstance().validateUser(mUsername)

    fun isUserAvaileble(): LiveData<List<User>> {
        return userObservable
    }

    class Factory(private val application: Application, private val username: String) :
        ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {

            return LoginViewModel(application, username) as T
        }
    }
}