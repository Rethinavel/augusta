package com.android.augusta.webservice.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.augusta.app.AugustaApp
import com.android.augusta.webservice.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepository {

    private var apiRequests: ApiRequests? = null

    init {
        apiRequests = ApiRequests.Factory.create(AugustaApp.applicationContext())
    }

    companion object {
        private var userRepository: UserRepository? = null
        @Synchronized
        @JvmStatic
        fun getInstance(): UserRepository {
            if (userRepository == null) {
                userRepository = UserRepository()
            }
            return userRepository!!
        }
    }

    fun validateUser(mUserName: String): LiveData<List<User>> {

        val mResult = MutableLiveData<List<User>>()
        apiRequests?.hasUserAvailable(mUserName)?.enqueue(object : Callback<List<User>> {

            override fun onResponse(
                call: Call<List<User>>,
                response: Response<List<User>>
            ) {
                mResult.value = response.body()
            }

            override fun onFailure(call: Call<List<User>>, t: Throwable) {
                mResult.value = null
            }

        })
        return mResult
    }
}