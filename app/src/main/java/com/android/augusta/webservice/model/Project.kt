package com.android.augusta.webservice.model

data class Project(
    var id: Long = 0,
    var name: String,
    var full_name: String? = null,
    var owner: User? = null,
    var description: String? = null,
    var url: String? = null,
    var git_url: String? = null,
    var stargazers_count: Int = 0,
    var watchers_count: Int = 0,
    var language: String? = null,
    var has_issues: Boolean = false,
    var forks_count: Int = 0,
    var open_issues_count: Int = 0,
    var forks: Int = 0
)
