package com.android.augusta.webservice.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.augusta.app.AugustaApp
import com.android.augusta.webservice.model.Project
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ProjectRepository {

    private var apiRequests: ApiRequests? = null

    init {
        apiRequests = ApiRequests.Factory.create(AugustaApp.applicationContext())
    }

    companion object {
        private var projectRepository: ProjectRepository? = null
        @Synchronized
        @JvmStatic
        fun getInstance(): ProjectRepository {
            if (projectRepository == null) {
                projectRepository = ProjectRepository()
            }
            return projectRepository!!
        }
    }

    fun getProjectList(userId: String): LiveData<List<Project>> {

        val data = MutableLiveData<List<Project>>()

        apiRequests?.getProjectList(userId)?.enqueue(object : Callback<List<Project>> {
            override fun onResponse(call: Call<List<Project>>, response: Response<List<Project>>) {
                data.value = response.body()
            }

            override fun onFailure(call: Call<List<Project>>, t: Throwable) {
                data.value = null
            }
        })

        return data
    }

    fun getProjectDetails(userID: String, projectName: String): LiveData<Project> {
        val data = MutableLiveData<Project>()

        apiRequests?.getProjectDetails(userID, projectName)?.enqueue(object : Callback<Project> {
            override fun onResponse(call: Call<Project>, response: Response<Project>) {
                data.value = response.body()
            }

            override fun onFailure(call: Call<Project>, t: Throwable) {
                data.value = null
            }
        })
        return data
    }


}