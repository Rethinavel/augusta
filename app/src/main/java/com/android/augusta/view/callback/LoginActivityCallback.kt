package com.android.augusta.view.callback

import android.view.View

interface LoginActivityCallback {
    fun onLoginClick(view: View)
    fun onRegisterClick(view: View)
}