package com.android.augusta.view.callback

import android.view.View

interface RegisterActivityCallback {
    fun onRegisterClick(view: View)
}