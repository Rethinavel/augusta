package com.android.augusta.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.augusta.R
import com.android.augusta.databinding.AdapterProjectListItemBinding
import com.android.augusta.view.callback.ProjectItemCallback
import com.android.augusta.webservice.model.Project

class ProjectListAdapter(
    private val projectList: MutableList<Project>,
    private val projectItemCallback: ProjectItemCallback
) : RecyclerView.Adapter<ProjectListAdapter.ProjectViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
        val binding = DataBindingUtil.inflate<AdapterProjectListItemBinding>(
            LayoutInflater.from(parent.context), R.layout.adapter_project_list_item,
            parent, false
        )
        binding.callback = projectItemCallback
        return ProjectViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return projectList.size
    }

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
        holder.binding.project = projectList.get(position)
        holder.binding.executePendingBindings()
    }

    class ProjectViewHolder(val binding: AdapterProjectListItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}


