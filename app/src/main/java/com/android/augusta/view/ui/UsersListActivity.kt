package com.android.augusta.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.augusta.R
import com.android.augusta.databinding.ActivityUsersListBinding
import com.android.augusta.view.adapter.UserListAdapter
import com.android.augusta.view.callback.UserItemCallback
import com.android.augusta.viewmodel.UserListViewModel
import com.android.augusta.webservice.model.User
import kotlinx.android.synthetic.main.activity_users_list.*


class UsersListActivity : AppCompatActivity() {

    private var binding: ActivityUsersListBinding? = null
    private lateinit var userAdapter: UserListAdapter
    private lateinit var recyclerLayoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_users_list)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_user_list)
        val viewModel = ViewModelProviders.of(this).get(UserListViewModel::class.java)

        binding?.userViewModel = viewModel
        binding?.isLoading = true

        setupRecyclerView()
        observeViewModel(viewModel)
    }

    fun setupRecyclerView() {

        userAdapter = UserListAdapter(userListItemClickCallback)
        recyclerLayoutManager = LinearLayoutManager(this)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = recyclerLayoutManager
            adapter = userAdapter
        }
    }

    private val userListItemClickCallback = object : UserItemCallback {

        override fun onClick(view: View, user: User) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                startActivity(
                    Intent(
                        this@UsersListActivity,
                        ProjectListActivity::class.java
                    ).putExtra("EXTRA_REPOSITORY_NAME", user.login)
                )
            }
        }
    }

    private fun observeViewModel(viewModel: UserListViewModel) {
        viewModel.userPagedList.observe(this, Observer {
            binding?.isLoading = false
            userAdapter.submitList(it)
        })
    }
}