package com.android.augusta.view.callback

import android.view.View
import com.android.augusta.webservice.model.User

interface UserItemCallback {
    fun onClick(view: View, user: User)
}
