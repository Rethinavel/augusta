package com.android.augusta.view.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.augusta.R
import com.android.augusta.databinding.ActivityLoginBinding
import com.android.augusta.view.callback.LoginActivityCallback
import com.android.augusta.viewmodel.LoginViewModel


class LoginActivity : AppCompatActivity(), LoginActivityCallback {

    private var activityLoginBinding: ActivityLoginBinding? = null
    private var userViewModel: LoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        activityLoginBinding?.loginActivityCallback = this
    }

    override fun onLoginClick(view: View) {
        observeLogin(
            activityLoginBinding?.edtEmail?.text.toString(),
            activityLoginBinding?.edtPassword?.text.toString()
        )
    }

    override fun onRegisterClick(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    private fun observeLogin(email: String, password: String) {

        val factory = LoginViewModel.Factory(application, email)
        userViewModel = ViewModelProviders.of(this, factory).get(LoginViewModel::class.java)

        userViewModel?.isUserAvaileble()?.observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                val mainIntent = Intent(this, UsersListActivity::class.java)
                startActivity(mainIntent)
            }
        })
    }
}
