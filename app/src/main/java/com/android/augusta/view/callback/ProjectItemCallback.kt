package com.android.augusta.view.callback

import android.view.View
import com.android.augusta.webservice.model.Project

interface ProjectItemCallback {
    fun onClick(view: View, project: Project)
}