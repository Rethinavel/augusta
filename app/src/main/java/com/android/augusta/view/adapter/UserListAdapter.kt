package com.android.augusta.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.android.augusta.R
import com.android.augusta.databinding.AdapterUserListItemBinding
import com.android.augusta.view.callback.UserItemCallback
import com.android.augusta.webservice.model.User


class UserListAdapter(private val userItemCallback: UserItemCallback) :
    PagedListAdapter<User, UserListAdapter.UserViewHolder>(USER_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val binding = DataBindingUtil.inflate<AdapterUserListItemBinding>(
            LayoutInflater.from(parent.context), R.layout.adapter_user_list_item,
            parent, false
        )
        binding.callback = userItemCallback
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.binding.user = getItem(position)
        holder.binding.executePendingBindings()
    }

    class UserViewHolder(val binding: AdapterUserListItemBinding) :
        RecyclerView.ViewHolder(binding.root)


    companion object {
        private val USER_COMPARATOR = object : DiffUtil.ItemCallback<User>() {
            override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
                newItem == oldItem
        }
    }
}


//class UserListAdapter(
//    private val usersList: MutableList<User>,
//    private val userItemCallback: UserItemCallback
//) : RecyclerView.Adapter<UserListAdapter.ProjectViewHolder>() {
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
//        val binding = DataBindingUtil.inflate<AdapterUserListItemBinding>(
//            LayoutInflater.from(parent.context), R.layout.adapter_user_list_item,
//            parent, false
//        )
//        binding.callback = userItemCallback
//        return ProjectViewHolder(binding)
//    }
//
//    override fun getItemCount(): Int {
//        return usersList.size
//    }
//
//    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
//        holder.binding.user = usersList.get(position)
//        holder.binding.executePendingBindings()
//    }
//
//    class ProjectViewHolder(val binding: AdapterUserListItemBinding) :
//        RecyclerView.ViewHolder(binding.root)
//}

