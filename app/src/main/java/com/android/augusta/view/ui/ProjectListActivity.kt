package com.android.augusta.view.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.augusta.R
import com.android.augusta.databinding.ActivityProjectListBinding
import com.android.augusta.view.adapter.ProjectListAdapter
import com.android.augusta.view.callback.ProjectItemCallback
import com.android.augusta.viewmodel.ProjectListViewModel
import com.android.augusta.webservice.model.Project
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_project_list.*
import kotlinx.android.synthetic.main.activity_users_list.*
import kotlinx.android.synthetic.main.activity_users_list.recyclerView
import kotlinx.android.synthetic.main.activity_users_list.toolbar


class ProjectListActivity : AppCompatActivity() {

    private var projectList = mutableListOf<Project>()
    private var binding: ActivityProjectListBinding? = null
    private lateinit var projectListAdapter: RecyclerView.Adapter<*>
    private lateinit var recyclerLayoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_project_list)

        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_project_list)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val mRepositoryName = intent.getStringExtra("EXTRA_REPOSITORY_NAME")
        val factory = ProjectListViewModel.Factory(application, mRepositoryName!!)
        val viewModel = ViewModelProviders.of(this, factory).get(ProjectListViewModel::class.java)

        binding?.projectViewModel = viewModel
        binding?.isLoading = true

        setupRecyclerView()
        observeViewModel(viewModel)
    }

    fun setupRecyclerView() {
        projectListAdapter = ProjectListAdapter(projectList, onProjectClicked)
        recyclerLayoutManager = LinearLayoutManager(this)
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = recyclerLayoutManager
            adapter = projectListAdapter
        }
    }

    private val onProjectClicked = object : ProjectItemCallback {
        override fun onClick(view: View, project: Project) {
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                Toast.makeText(
                    this@ProjectListActivity, getString(R.string.app_name), Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun observeViewModel(viewModel: ProjectListViewModel) {
        viewModel.getProjectListObservable().observe(this, Observer<List<Project>> { mItems ->

            binding?.isLoading = false

            if (mItems != null && mItems.isNotEmpty()) {
                projectList.clear()
                projectList.addAll(mItems)
                projectListAdapter.notifyDataSetChanged()
            }else
                Snackbar.make(parentLayout, getString(R.string.error_msg_no_projects_found), Snackbar.LENGTH_SHORT).show()
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

}