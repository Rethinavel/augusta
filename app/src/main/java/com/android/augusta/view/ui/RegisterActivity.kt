package com.android.augusta.view.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.android.augusta.R
import com.android.augusta.databinding.ActivityRegisterBinding
import com.android.augusta.view.callback.RegisterActivityCallback
import com.android.augusta.viewmodel.LoginViewModel
import com.android.augusta.viewmodel.UserListViewModel
import com.android.augusta.webservice.model.User

class RegisterActivity : AppCompatActivity(), RegisterActivityCallback {
    lateinit var activityRegisterBinding: ActivityRegisterBinding
    lateinit var userViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        activityRegisterBinding.registerActivityCallback = this

        val viewModel = ViewModelProviders.of(this).get(UserListViewModel::class.java)


    }

    override fun onRegisterClick(view: View) {

    }

    private fun observeRegister(user: User) {

    }

}
