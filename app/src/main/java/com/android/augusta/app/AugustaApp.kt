package com.android.augusta.app

import android.app.Application
import android.content.Context


class AugustaApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: AugustaApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }


}

