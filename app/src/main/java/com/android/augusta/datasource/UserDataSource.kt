package com.android.augusta.datasource

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.android.augusta.app.AugustaApp
import com.android.augusta.utils.Appconstants
import com.android.augusta.webservice.model.User
import com.android.augusta.webservice.repository.ApiRequests
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserDataSource : PageKeyedDataSource<Int, User>() {

    private var apiRequests: ApiRequests? = null

    init {
        apiRequests = ApiRequests.Factory.create(AugustaApp.applicationContext())
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, User>
    ) {
        apiRequests?.getUserList(Appconstants.RECORDS_PER_PAGE)
            ?.enqueue(object : Callback<List<User>> {
                override fun onResponse(
                    call: Call<List<User>>,
                    response: Response<List<User>>
                ) {
                    val apiResponse = response.body()!!
                    apiResponse.let {
                        callback.onResult(apiResponse, 25, 25)
                    }
                }

                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    Log.e("OIn Failure API call", t.message!!)
                }
            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        Log.e("loadAfter", "Params " + params.key)

        apiRequests?.getUserList(params.key + Appconstants.RECORDS_PER_PAGE)
            ?.enqueue(object : Callback<List<User>> {
                override fun onResponse(
                    call: Call<List<User>>,
                    response: Response<List<User>>
                ) {
                    val apiResponse = response.body()!!
                    apiResponse.let {
                        callback.onResult(apiResponse, params.key + Appconstants.RECORDS_PER_PAGE)
                    }
                }

                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    Log.e("OIn Failure API call", t.message!!)
                }
            })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, User>) {
        Log.e("loadBefore", "Params " + params.key)
    }
}